package eu.fr.uchitesting.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import eu.fr.uchitesting.model.Alien;

public class AlienDao {
	private Connection connection;

	private Connection getConnection() {
		ResourceBundle dbResources = null;
		try {
//			dbResources = ResourceBundle.getBundle("dbConfig");
			dbResources = ResourceBundle.getBundle("eu.fr.uchitesting.db.dbConfig");
		} catch (MissingResourceException e) {
			System.err.println("Database configuration bundle not found.");
		}

		String driver = dbResources.getString("driver");

		String protocol = dbResources.getString("protocol");
		String domain = dbResources.getString("domain");
		String port = dbResources.getString("port");
		String dbName = dbResources.getString("dbName");
		String url = protocol + "://" + domain + ":" + port + "/" + dbName;

		String username = dbResources.getString("username");
		String password = dbResources.getString("password");

		Connection connection = null;

		try {
			Class.forName(driver);
			connection = DriverManager.getConnection(url, username, password);
		} catch (ClassNotFoundException e) {
			System.err.println("Le driver " + driver + " n'a pas �t� trouv�.");
		} catch (SQLException e) {
			System.err.println("Echec de connexion");
			e.printStackTrace();
		}
		return connection;

	}

	public Alien getAlien(int aid) {
		Alien alien = new Alien();

		try {
			if (connection == null)
				connection = getConnection();

			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery("SELECT * FROM alien WHERE aid=" + aid);

			if (rs.next()) {
				alien.setAid(rs.getInt("aid"));
				alien.setAname(rs.getString("aname"));
				alien.setTech(rs.getString("tech"));
			}

			rs.close();
			st.close();
			connection.close();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.err.println(e.getCause());
			e.printStackTrace();
		}

		return alien;
	};

}
