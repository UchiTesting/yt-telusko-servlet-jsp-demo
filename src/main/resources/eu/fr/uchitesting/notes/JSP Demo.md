JSP Demo
========

This is based on instruction found on the YT video [Servlet | JSP | JDBC | Maven Example](https://youtu.be/WXmTxgDg52o) by user Telusko. A GIT repo related to that video can be found [here](https://github.com/navinreddy20/youtubeProjects/tree/master/ServletJspDemo).

It will not necessarily replicate the steps shown there. Some personal research and actions are taken to make things work as needs be.

## Project Setup

### Create the Project

Either go with:

- *Dynamic Web Project*
- *Maven project*

The video chose the later which is what I am after.

Create a new maven project.
We'll pick the *maven-archetype-webapp* archetype from internal catlog.

Provide the info.

Mine will be:

- *Group Id*: eu.fr.uchitesting
- *Artifact Id*: ServletJspDemo

Click Finish


### You're a Java dev. You just can't have it too easy. A bunch of problems for you.

The archetype is actually broken / outdated. Compiling from it leads to a set of errors.

#### `HttpServlet` class not found
The JSP page provided has this message

```
The superclass "jakarta.servlet.http.HttpServlet" was not found on the Java Build Path	index.jsp	/ServletJspDemo/src/main/webapp	line 1
```

This is apparently because this class is on the server. As soon as we link the project to a server, this error disappears.

In project properties go to Targeted Runtimes and check an available server.

Works for me.

#### No *src/main/java* folder

Just create it manually. Not a package. A folder.

If this is not enough go to project properties and in *Project Facets* deselect and reselect *Dynamic Web Module*. Those steps my be applied each.

Another place to check is *Java Build Path* and make sure the folder is set a source folder.

#### Compilation failure

Upon compiling

```log
Failed to execute goal org.apache.maven.plugins:maven-war-plugin:2.2:war (default-war) on project ServletJspDemo: Execution default-war of goal org.apache.maven.plugins:maven-war-plugin:2.2:war failed: Unable to load the mojo 'war' in the plugin 'org.apache.maven.plugins:maven-war-plugin:2.2' due to an API incompatibility: org.codehaus.plexus.component.repository.exception.ComponentLookupException: Cannot access defaults field of Properties
```

A [help link](https://cwiki.apache.org/confluence/display/MAVEN/PluginContainerException) provided in the message tells about possible reasons this happens.

- Dependencies got corrupted.
- Asking for incompatible dependencies.
- Plugin improperly assembled by the creator

An action to perform is to clean related files under the *.m2* folder in the user directory.

Unfortunately it does not work for me. The same error appears.

Stack Overflow however states a newer version of the plugin shall be used.

Add the dependency

```xml
<!-- https://mvnrepository.com/artifact/org.apache.maven.plugins/maven-war-plugin -->
<dependency>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-war-plugin</artifactId>
    <version>3.3.2</version>
</dependency>

```

And the plugin information
```xml
<build>
    <plugins>
        <plugin>
            <artifactId>maven-war-plugin</artifactId>
            <version>3.3.2</version>
        </plugin>
    </plugins>
</build>
```

At this stage the project builds for me.

#### Error 404 on Servlet with good configuration

Happened to me the servlet returned error 404 or Error 500 with exceptions.
Tomcat log showed it was marking the servlet as unavailable. Some research told :

- Servlet must be places in a package.
- *.class* files should be placed under *WEB-INF/classes* folder.
- It happens when the servlet has none of `doGet()` or `doPost()` methods.

The thing is my project had all the above compliant yet nothing worked.
Did clean Tomcat and Maven clean install several times on the project with no apparent result. Ultimately, I removed the heading slash on the url-pattern tag into *web.xml* file. It made Tomcat fail at startup. The log were clear about a faulty url-pattern so I put it back. Tomcat starts. I give it a try with no hope but it worked.

### Add other dependencies

> MariaDB Driver

The video is working with the MySQL Connector and it should work with MariaDB as well. I will use the MariaDB driver instead.

> MariaDB client

```xml
<!-- https://mvnrepository.com/artifact/org.mariadb.jdbc/mariadb-java-client -->
<dependency>
    <groupId>org.mariadb.jdbc</groupId>
    <artifactId>mariadb-java-client</artifactId>
    <version>3.0.8</version>
</dependency>
```

> Jakarta Servlet API

```xml
<!-- https://mvnrepository.com/artifact/jakarta.servlet/jakarta.servlet-api -->
<dependency>
    <groupId>jakarta.servlet</groupId>
    <artifactId>jakarta.servlet-api</artifactId>
    <version>6.0.0</version>
    <scope>provided</scope>
</dependency>
```

## The actual code

> I did perform the steps along with the video. For the sake of organising I did rework the order of operations.  
> As I was taught earlier: do things in the order of MVC. First setup the **M**odel, then the **V**iew and ultimately the **C**ontroller.

### Create a Model

#### The Business Model

In a package I chose to be `eu.fr.uchitesting.model`, I create an `Alien` type.

It will have simple data

| Attribute | Type | Description |
|-|-|-|
| aid | `int` | Integer value used as primary key in the related table in DB |
| aname | `String` | Simply a name. In the DB I did set this to `NOT NULL` |
| tech | `String` | A tech name |
 
 ```java
package eu.fr.uchitesting.model;

public class Alien {
	private int aid;
	private String aname;
	private String tech;

	public int getAid() {
		return aid;
	}

	public void setAid(int aid) {
		this.aid = aid;
	}

	public String getAname() {
		return aname;
	}

	public void setAname(String aname) {
		this.aname = aname;
	}

	public String getTech() {
		return tech;
	}

	public void setTech(String tech) {
		this.tech = tech;
	}

	@Override
	public String toString() {
		return "Alien [aid=" + aid + ", aname=" + aname + ", tech=" + tech + "]";
	}
}
 ```

#### The DAO

To deal with DB interaction we create a <abbr title="Data Access Object">DAO</abbr>. It will be responsible to manage <abbr title="Create Read Update Delete">CRUD</abbr> operations.

```java
package eu.fr.uchitesting.dao;

import eu.fr.uchitesting.model.Alien;

public class AlienDao {
	
	public Alien getAlien(int aid) {
		Alien alien = new Alien();
		alien.setAid(101);
		alien.setAname("Navin");
		alien.setTech("Java");
		
		return alien;
	};

}
```

### Update the view

We plan to send an number corresponding to an alien ID in a form and display the corresponding Alien in a receiving page.

#### Form

We'll simply add a simple form to ask for an Alien ID (`aid` attribute from our `Alien` type) in *index.jsp* file.

```jsp
<form action="alien">
    <input type="text" name="aid"/><br />
    <input type="submit" />
</form>
```

#### Result page

We will have a specific page to displays the result. We create a *showAlien.jsp* file.

In it we will create a scriptlet displaying the `alien` object.

```jsp
<%
// Scriptlet
Alien alien = (Alien) request.getAttribute("alien");
out.println(alien);
%>
```

### Add `AlienController` Servlet

#### Create the Servlet (Controller)

On the source folder *src/main/java* create a package to hold servlets

I create the package *eu.fr.uchitesting.servlet*. In there I create a new *AlienController* servlet.

On a first screen, we name our servlet. I chose *AlienController*. Just a remainder from ASP .NET. Also I don't want to rename the class should I explore further.

![Servlet Wizard Page 1](<./img/Eclipse_Create_Servlet_1.png>)

The next page allows to define the URL mappings. I pick */Alien* for the same reason as above.
At this stage we can click Finish.

![Servlet Wizard Page 2](<./img/Eclipse_Create_Servlet_2.png>)

Optionally a further page allows us to pick which methods to implement

![Servlet Wizard Page 3](<./img/Eclipse_Create_Servlet_3.png>)

I met a problem with some classes not resolved in which `javax.servlet.http.HttpServletRequest`. Replaced `javax` with `jakarta` on them.

The *web.xml* file is also completed.

```xml
<!DOCTYPE web-app PUBLIC
 "-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN"
 "http://java.sun.com/dtd/web-app_2_3.dtd" >

<web-app>
	<display-name>Archetype Created Web Application</display-name>
	<servlet>
		<servlet-name>AlienController</servlet-name>
		<display-name>AlienController</display-name>
		<description></description>
		<servlet-class>eu.fr.uchitesting.servlet.AlienController</servlet-class>
	</servlet>
	<servlet-mapping>
		<servlet-name>AlienController</servlet-name>
		<url-pattern>/Alien</url-pattern>
	</servlet-mapping>
</web-app>
```

> Though I did not pay attention the exact moment it changed, at this stage I notice there is no more errors raised by Eclipse.

#### Edit the Servlet

We'll simply fetch an `aid` integer value. This attribute comes from our database.

```java
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	int aid = Integer.parseInt(request.getParameter("aid"));
	AlienDao dao = new AlienDao();
	// For now the value here is ignored as DAO returns an hard-coded instance
	Alien alien = dao.getAlien(aid);
	
	request.setAttribute("alien", alien);
	
	RequestDispatcher rd = request.getRequestDispatcher("showAlien.jsp");
	rd.forward(request, response);
}
```

> Trying the Project available at [http://localhost:8080/ServletJspDemo/](http://localhost:8080/ServletJspDemo/) gives back our dummy `Alien`.

### Get Actual Data from the DB

The DAO sends us a dummy Alien as plumbing code. As it shows working, we now may develop the actual JDBC code. The `getConnection()` method also belongs to the DAO. 
> Check the sources for the details. It basically loads the connection settings stored into *dbConfig.properties* located in the *eu.fr.uchitesting.db* package.

> *dbConfig.properties* (Should be created manually as it is in *.gitignore*)

```
driver=org.mariadb.jdbc.Driver
protocol=jdbc:mariadb
domain=localhost
port=3306
dbName=navin
username=yourUserName
password=yourPassword
```


```java
	public Alien getAlien(int aid) {
		Alien alien = new Alien();

		try {
			if (connection == null)
				connection = getConnection();

			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery("SELECT * FROM alien WHERE aid=" + aid);

			if (rs.next()) {
				alien.setAid(rs.getInt("aid"));
				alien.setAname(rs.getString("aname"));
				alien.setTech(rs.getString("tech"));
			}

			rs.close();
			st.close();
			connection.close();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.err.println(e.getCause());
			e.printStackTrace();
		}

		return alien;
	};
```
