CREATE DATABASE navin;

USE navin;

CREATE TABLE alien (
	aid INT AUTO_INCREMENT PRIMARY KEY,
	aname VARCHAR(50) NOT NULL,
	tech VARCHAR(50)
)ENGINE=InnoDB;

INSERT INTO alien (aname, tech)
VALUES
('Navin','Jave'),
('Archana','Big Data'),
('David','.NET'),
('Anna','ML'),
('Pranav','Oracle'),
('Kiran','DS'),
('Maria','Hardware'),
('Kamil','Java'),
('Me','.NET'),
('You','UI/UX'),
('Them','Usage');